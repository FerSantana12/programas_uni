﻿<?php

    function getPlantilla($sql, $array, $array_study, $array_reference){
        $plantilla = '<body>

            <div class="wraper">
            
                <aside class="aside aside1">
                    <p>'.$sql["telefono"].'</p>
                    <p>'.$sql["correo"].'</p>
                    <p>Managua</p>
                </aside>

                <aside class="aside line"></aside>

                <header class="header">
                    <h1>'.$sql['nombre'].'</h1>
                    <h1>'.$sql['apellidos'].'</h1>
                    <p>'.$sql['cedula'].'</p>
                </header>
                
                <hr>

                <aside class="main2">
                    <p>Capacidad para trabajar en equipo, buena presentacion, honesto, responsable.</p>
                </aside>

                <article class="photo">
                    <img src="'.$sql['img'].'" alt="" style="width:150; height:130">
                </article>

                <hr>
                
                <aside class="aside aside2">
                    <p id="rightDetail" class="detalleTexto">Formacion academica</p>';
                    
                    foreach($array_study as $key => $value){    
                        $plantilla .= '<p><strong>'.$value["Carrera estudiada"].'</strong></p>
                        
                        <p class="pSmaller">'.$value["Institucion"].'</p>                        
                        <p class="pSmaller">'.$value["Especialidad"].'</p>
                        <p class="pSmaller">'.$value["Fecha de Finalizacion"].'</p>';
                      }
                    
                $plantilla .= '<br><p id="rightDetail" class="detalleTexto">Referencias</p>';

                    foreach($array_reference as $key => $value){
                        $plantilla .= '<p class="pSmaller"><strong>'.$value["Nombre"].'</strong> '.$value["Telefono"].'</p>';
                    }
                
                $plantilla .='</aside>
                <aside class="aside line"></aside>        
                
                <article class="main">
                    
                    <p id="detail" class="detalleTexto">Experiencias De Trabajo</p>';

                    foreach($array as $key => $value){    
                        $plantilla .= '<strong>'.$value["Cargo Desempeñado"].'</strong>

                        <p class="p margen"><small>'.$value["Compañia"].' | '.$value["Desde"].' - '.$value["Hasta"].'</small></p>                        
                                                            
                        <p class="p">'.$value["Descripcion"].'</p>';
                      }    
            $plantilla  .='</article>

            <footer>
                <a title="LinkedIn" href="https://es.linkedin.com/"><img src="../img/linkedin-link.png" alt="LinkedIn"/></a>
                <a title="NicaEmpleos" href="http://nicaempleo.com/"><img src="../img/website-link.png" alt="NicaEmpleos" align="right"></a>
            </footer>
            </div>
        </body>';

        return $plantilla;
    }
    
?>