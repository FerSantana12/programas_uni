<?php

function getSql(){
    try{
        $base = new PDO('mysql:host=localhost; dbname=prueba', 'root', '');
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $base->exec("SET CHARACTER SET UTF8");
        $sql=$base->query("SELECT * FROM registro Where id=2")->fetch();

    }catch(Exception $e){
        die('Error: '. $e->GetMessage());
    }finally{
        $base = null;
    }

    return $sql;
}

function getExperience(){
    try{
        $base = new PDO('mysql:host=localhost; dbname=prueba', 'root', '');
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $base->exec("SET CHARACTER SET UTF8");
        $sql_json=$base->query("SELECT * FROM tbl_prueba_fernando WHERE id_deta_can = 2")->fetch();

        $codificado = $sql_json['experience'];
        $array = json_decode($codificado, true);


    }catch(Exception $e){
        die('Error: '. $e->GetMessage());
    }finally{
        $base = null;
    }

    return $array;
}

function getStudy(){
    try{
        $base = new PDO('mysql:host=localhost; dbname=prueba', 'root', '');
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $base->exec("SET CHARACTER SET UTF8");
        $sql_json=$base->query("SELECT * FROM tbl_prueba_fernando WHERE id_deta_can = 2")->fetch();

        $codificado = $sql_json['study'];
        $array_study=json_decode($codificado, true);


    }catch(Exception $e){
        die('Error: '. $e->GetMessage());
    }finally{
        $base = null;
    }

    return $array_study;
}

function getReference(){
    try{
        $base = new PDO('mysql:host=localhost; dbname=prueba', 'root', '');
        $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $base->exec("SET CHARACTER SET UTF8");
        $sql_json=$base->query("SELECT * FROM tbl_prueba_fernando WHERE id_deta_can = 2")->fetch();

        $codificado_reference = $sql_json['reference'];
        $array_reference = json_decode($codificado_reference, true);


    }catch(Exception $e){
        die('Error: '. $e->GetMessage());
    }finally{
        $base = null;
    }
    
    return $array_reference;
}

?>