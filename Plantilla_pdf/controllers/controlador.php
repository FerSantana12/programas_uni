<?php
    require_once('../models/cv_pdf.php');

    $sql = getSql();
    $array = getExperience();
    $array_study = getStudy();
    $array_reference = getReference();
    
    require_once('../vendor/autoload.php');
    require_once('../views/pdfprueba.php');
    $css =file_get_contents('../css/estilos.css');

    $mpdf = new \Mpdf\Mpdf([

    ]);

    $plantilla = getPlantilla($sql, $array, $array_study, $array_reference);
   
    $mpdf->WriteHtml($css, \Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHtml($plantilla, \Mpdf\HTMLParserMode::HTML_BODY);
     
    $mpdf->Output();
    
?>