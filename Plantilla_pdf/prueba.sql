-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-02-2021 a las 22:34:03
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE `registro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `correo` varchar(35) NOT NULL,
  `cedula` varchar(30) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `img` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`id`, `nombre`, `apellidos`, `correo`, `cedula`, `telefono`, `sexo`, `img`) VALUES
(1, 'Fernando Joel', 'Valdez Santana', 'fjsantana123@gmail.com', '001-283675-7521E', '84904371', 'M', '../img/perfil.png'),
(2, 'Carlos Jose', 'Gutierrez Lopez', 'carlosjose@gmail.com', '001-376544-1234E', '54793841', 'M', '../img/perfil.png'),
(3, 'Martin Joel', 'Ruiz Ruiz', 'martinrr@gmail.com', '001-283675-4212B', '82713841', 'M', '../img/perfil.png'),
(4, 'Andres Juan', 'Rodriguez Rodriguez', 'andresjuan@gmail.com', '001-283675-7521E', '81914121', 'M', '../img/perfil.png'),
(5, 'Rodrigo Victor', 'Gonzalez Gonzalez', 'rodrigov@gmail.com', '001-283675-4212B', '84904371', 'M', '../img/perfil.png'),
(6, 'Luis Marcos', 'Mejia Mejia', 'luismarcos@gmail.com', '001-283675-7521E', '50863811', 'M', '../img/perfil.png'),
(7, 'Maria Alicia', 'Martinez Martinez', 'mariamartinez@gmail.com', '001-283675-4212B', '82713841', 'F', '../img/perfil.png'),
(8, 'Monica Rosario', 'Perez Perez', 'monicaperez@gmail.com', '001-283675-7521E', '84904371', 'F', '../img/perfil.png'),
(9, 'Paula Luisa', 'Garay Garay', 'paulagaray@gmail.com', '001-376544-1234E', '50863811', 'F', '../img/perfil.png'),
(10, 'Gustavo Tomas', 'Herrera Herrera', 'gustavoherrera@gmail.com', '001-376544-1234E', '54793841', 'M', '../img/perfil.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_prueba_fernando`
--

CREATE TABLE `tbl_prueba_fernando` (
  `id_deta_can` int(10) NOT NULL,
  `iducan` int(10) DEFAULT NULL,
  `cv_name` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `civil_status` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `sons` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `study` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `experience` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `reference` text COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_reg` int(10) DEFAULT NULL,
  `user_mod` int(10) DEFAULT NULL,
  `fecha_mod` datetime DEFAULT NULL,
  `fecha_reg` datetime DEFAULT NULL,
  `state` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_prueba_fernando`
--

INSERT INTO `tbl_prueba_fernando` (`id_deta_can`, `iducan`, `cv_name`, `civil_status`, `sons`, `study`, `experience`, `reference`, `user_reg`, `user_mod`, `fecha_mod`, `fecha_reg`, `state`) VALUES
(1, 14, 'CV01', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Estudiante Universitario (4to Año)\",\"Carrera estudiada\":\"Ingeniería en Sistemas\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"},{\"Nivel estudiado\":\"Estudiante Universitario (2do Año)\",\"Carrera estudiada\":\"Banca y Finanzas\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}, {\"Nivel estudiado\":\"Universidad completa | Graduado\",\"Carrera estudiada\":\"Mercadotecnia\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Centroamericana\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Bodega\",\"Compañia\":\"Gonper\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Etiquetar\"},{\"Cargo Desempeñado\":\"Imprenta\",\"Compañia\":\"La Prensa\",\"Desde\":\"2018-02-01\",\"Hasta\":\"2018-03-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Ensamblar periodicos\"},{\"Cargo Desempeñado\":\"Pasante\",\"Compañia\":\"Gicapp S.A.\",\"Desde\":\"2020-10-08\",\"Hasta\":\"2020-10-16\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Pasantias\"}]', '[{\"Nombre\":\"Elber Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Juan Perez\", \"Telefono\":\"82165390\"}]', 14, NULL, NULL, '2020-10-07 21:22:14', 'Activo'),
(2, 15, 'CV02', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Egresado\",\"Carrera estudiada\":\"Ingeniería en Computacion\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Nacional de Ingenieria\",\"Fecha de Finalizacion\":\"2020-01-01\"},{\"Nivel estudiado\":\"Estudiante Universitario (2do Año)\",\"Carrera estudiada\":\"Diseño Grafico\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}, {\"Nivel estudiado\":\"Universidad completa | Graduado\",\"Carrera estudiada\":\"Mercadotecnia\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Centroamericana\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Bodega\",\"Compañia\":\"Gonper\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Etiquetar\"},{\"Cargo Desempeñado\":\"Imprenta\",\"Compañia\":\"La Prensa\",\"Desde\":\"2018-02-01\",\"Hasta\":\"2018-03-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Ensamblar periodicos\"},{\"Cargo Desempeñado\":\"Pasante\",\"Compañia\":\"Gicapp S.A.\",\"Desde\":\"2020-10-08\",\"Hasta\":\"2020-10-16\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Pasantias\"}]', '[{\"Nombre\":\"Francisco Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Luis Castillo\", \"Telefono\":\"82165390\"}]', 15, NULL, NULL, '2020-10-07 21:22:14', 'Activo'),
(3, 16, 'CV03', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Egresado\",\"Carrera estudiada\":\"Contaduria\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Contador\",\"Compañia\":\"Gonper\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Registro Caja\"}]', '[{\"Nombre\":\"Elber Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Juan Perez\", \"Telefono\":\"82165390\"}]', 16, NULL, NULL, '0000-00-00 00:00:00', 'Activo'),
(4, 17, 'CV04', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Estudiante Universitario (4to Año)\",\"Carrera estudiada\":\"Administracion de empresas\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Gerente general\",\"Compañia\":\"Gonper\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Etiquetar\"}]', '[{\"Nombre\":\"Francisco Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Luis Castillo\", \"Telefono\":\"82165390\"}]', 17, NULL, NULL, '2020-10-07 21:22:14', 'Activo'),
(5, 18, 'CV05', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Estudiante Universitario (4to Año)\",\"Carrera estudiada\":\"Medicina\",\"Especialidad\":\"Medico Cirujano\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Medico\",\"Compañia\":\"Hospital Velez Paiz\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Etiquetar\"}]', '[{\"Nombre\":\"Elber Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Juan Perez\", \"Telefono\":\"82165390\"}]', 18, NULL, NULL, '2020-10-07 21:22:14', 'Activo'),
(6, 19, 'CV06', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Estudiante Universitario (4to Año)\",\"Carrera estudiada\":\"Ingeniería Civil\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Nacional de Ingenieria\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Pasante\",\"Compañia\":\"Gonper\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Pasantias\"}]', '[{\"Nombre\":\"Elber Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Juan Perez\", \"Telefono\":\"82165390\"}]', 19, NULL, NULL, '2020-10-07 21:22:14', 'Activo'),
(7, 20, 'CV07', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Estudiante Universitario (4to Año)\",\"Carrera estudiada\":\"Psicologia\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Psicologo\",\"Compañia\":\"Centro Clinico\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Etiquetar\"}]', '[{\"Nombre\":\"Elber Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Juan Perez\", \"Telefono\":\"82165390\"}]', 21, NULL, NULL, '2020-10-07 21:22:14', 'Activo'),
(8, 21, 'CV08', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Estudiante Universitario (4to Año)\",\"Carrera estudiada\":\"Enfermeria\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Enfermero/a\",\"Compañia\":\"Hospital Manolo Morales\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Etiquetar\"}]', '[{\"Nombre\":\"Francisco Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Luis Castillo\", \"Telefono\":\"82165390\"}]', 20, NULL, NULL, '2020-10-26 21:22:14', 'Activo'),
(9, 22, 'CV09', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Estudiante Universitario (4to Año)\",\"Carrera estudiada\":\"Arquitectura\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Bodega\",\"Compañia\":\"Gonper\",\"Desde\":\"2020-01-01\",\"Hasta\":\"2020-02-01\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Etiquetar\"}]', '[{\"Nombre\":\"Elber Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Juan Perez\", \"Telefono\":\"82165390\"}]', 22, NULL, NULL, '2020-10-26 21:22:14', 'Activo'),
(10, 23, 'CV10', 'Soltero/a', '0', '[{\"Nivel estudiado\":\"Egresado\",\"Carrera estudiada\":\"Ingeniería en Sistemas\",\"Especialidad\":\"Prueba\",\"Institucion\":\"Universidad Politécnica de Nicaragua\",\"Fecha de Finalizacion\":\"2020-01-01\"}]', '[{\"Cargo Desempeñado\":\"Pasante\",\"Compañia\":\"Gicapp S.A.\",\"Desde\":\"2020-10-08\",\"Hasta\":\"2020-10-16\",\"Moneda\":\"Cordobas\",\"Rango Salarial\":\"1 - 1000\",\"Beneficios\":[],\"Descripcion\":\"Pasantias\"}]', '[{\"Nombre\":\"Francisco Soliz Paiz\", \"Telefono\":\"57748154\"}, {\"Nombre\":\"Henry Martinez\", \"Telefono\":\"83306899\"}, {\"Nombre\":\"Luis Castillo\", \"Telefono\":\"82165390\"}]', 23, NULL, NULL, '2020-10-26 21:22:14', 'Activo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_prueba_fernando`
--
ALTER TABLE `tbl_prueba_fernando`
  ADD PRIMARY KEY (`id_deta_can`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tbl_prueba_fernando`
--
ALTER TABLE `tbl_prueba_fernando`
  MODIFY `id_deta_can` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
