jQuery(function() {
  var myOptions = {
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  map = new google.maps.Map(document.getElementById("map"), myOptions);

  address = 'managua'
  geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': address}, function(results) {
    map.setCenter(results[0].geometry.location);
  });

  $("#btnplay").click(function(){
    initialize();
  }); 
        
});

function initialize(){
  opcion = 1;
  var fecha = $("#fecha").val();
  if(fecha.length <= 0){
    alert("El campo date es obligatorio");
  }else{ 
    $.ajax({
      type: 'post',
      url: 'bd/crud.php',
      datatype: 'json',
      data: {fecha:fecha, opcion:opcion},
      success: function (data) {
        var data_coords =JSON.parse(data);
        var stops = [];
        data_coords.forEach(element => {
          stops.push({"Geometry":{ "Latitude": parseFloat(element.lat), "Longitude": parseFloat(element.lng)}});      
        });
        map = new window.google.maps.Map(document.getElementById("map"));
        
        var directionsDisplay = new window.google.maps.DirectionsRenderer({
          suppressMarkers: true
        });
        var directionsService = new window.google.maps.DirectionsService();

        Tour_startUp(stops);

        window.tour.loadMap(map, directionsDisplay);
        window.tour.fitBounds(map);

        if (stops.length > 1)
          window.tour.calcRoute(directionsService, directionsDisplay);
      }
    });
  }
}
function Tour_startUp(stops) {
  var step = 50; // 50; // metros
  var tick = 100; // milisegundos
  var eol, lastVertex = 1;
  var polyline = null, poly2 = null;
  var timerHandle = null;

  if (!window.tour) window.tour = {
    updateStops: function(newStops) {
      stops = newStops;
    },
    // map: objeto google map
    // directionsDisplay: objeto google directionsDisplay (viene vacio)
    loadMap: function(map, directionsDisplay) {
      var myOptions = {
        zoom: 13,
        center: new window.google.maps.LatLng(12.0859734,-86.3039478), // default to London
        mapTypeId: window.google.maps.MapTypeId.ROADMAP
      };
      map.setOptions(myOptions);
      directionsDisplay.setMap(map);
    },
    fitBounds: function(map) {
      var bounds = new window.google.maps.LatLngBounds();
      // limites extendidos para cada coordenada
      jQuery.each(stops, function(key, val) {
        var myLatlng = new window.google.maps.LatLng(val.Geometry.Latitude, val.Geometry.Longitude);
        bounds.extend(myLatlng);
      });
      map.fitBounds(bounds);
    },
    calcRoute: function(directionsService, directionsDisplay) {
      if(timerHandle){clearTimeout(timerHandle)};
      polyline = new google.maps.Polyline({
        path: [],
        strokeColor: '#FF0000',
        strokeWeight: 3
      });
      poly2 = new google.maps.Polyline({
          path: [],
          strokeColor: '#FF0000',
          strokeWeight: 3
      });
      var batches = [];
      var itemsPerBatch = 10; // google API max = 10 - 1 start, 1 stop, and 8 waypoints
      var itemsCounter = 0;
      var wayptsExist = stops.length > 0;
      while (wayptsExist) {
        var subBatch = [];
        var subitemsCounter = 0;
        for (var j = itemsCounter; j < stops.length; j++) {
          subitemsCounter++;
          subBatch.push({
            location: new window.google.maps.LatLng(stops[j].Geometry.Latitude, stops[j].Geometry.Longitude),
            stopover: true
          });
          if (subitemsCounter == itemsPerBatch)
            break;
        }
        itemsCounter += subitemsCounter;
        batches.push(subBatch);
        wayptsExist = itemsCounter < stops.length;
        // En la proxima iteracion todavía quedan puntos, menos uno.
        // entonces continua comenzando con la proxima coordenada
        itemsCounter--;
      }
      // ahora se declaran 2 arreglos que guardaran la lista de waypoints
      var combinedResults;
      var unsortedResults = [{}]; // con esta variable guardo los resultados a como vienen, para luego ordenarlos
      var directionsResultsReturned = 0;
      for (var k = 0; k < batches.length; k++) {
        var lastIndex = batches[k].length - 1;
        var start = batches[k][0].location;
        var end = batches[k][lastIndex].location;
        // recortar la primera y la ultima entrada de la matriz, ya que esas son origen y destino
        var waypts = [];
        waypts = batches[k];
        waypts.splice(0, 1);
        waypts.splice(waypts.length - 1, 1);
        var request = {
          origin: start,
          destination: end,
          waypoints: waypts,
          travelMode: window.google.maps.TravelMode.DRIVING
        };
        (function(kk) {
          directionsService.route(request, function(result, status) {
            if (status == window.google.maps.DirectionsStatus.OK) {
              var unsortedResult = {
                order: kk,
                result: result
              };
              unsortedResults.push(unsortedResult);
              directionsResultsReturned++;
              if (directionsResultsReturned == batches.length) // cuando se reciban todos los resultados se insertan al mapa
              {
                // aqui comienza el ordenamiento de los valores retornados
                unsortedResults.sort(function(a, b) {
                  return parseFloat(a.order) - parseFloat(b.order);
                });
                var count = 0;
                for (var key in unsortedResults) {
                  if (unsortedResults[key].result != null) {
                    if (unsortedResults.hasOwnProperty(key)) {
                      if (count == 0) // el primer resultado con el que se inicializa el objeto combinedResults
                        combinedResults = unsortedResults[key].result;
                      else {
                        //Solo se guardan los legs, overview_path y bounds en el objeto. No es un objeto directionsResults completo
                        //pero suficiente como para dibujar las direcciones en el mapa por donde va pasar la animacion, que es todo lo que necesito
                        combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                        combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);
                        combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                        combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                      }
                      count++;
                    }
                  }
                }
                directionsDisplay.setDirections(combinedResults);
                var bounds = new google.maps.LatLngBounds();
                endLocation =  new Object();
                var legs = combinedResults.routes[0].legs;
                console.log(legs);
                for (var i = 0; i < legs.length; i++) {
                  if(i==0){
                    myMarker = createMarker(directionsDisplay.getMap(),legs[i].start_location," ",legs[i].start_address);
                  }
                  endLocation.latlng = legs[i].end_location;
                  var steps = legs[i].steps;
                  for (j=0;j<steps.length;j++) {
                    var nextSegment = steps[j].path;
                    for (k=0;k<nextSegment.length;k++) {
                      polyline.getPath().push(nextSegment[k]);
                      bounds.extend(nextSegment[k]);
                    }
                  }
                  createMarker(directionsDisplay.getMap(), legs[i].start_location, i, "some text for marker " + i + "<br>" + legs[i].start_address);
                }
                var i = legs.length;
                createMarker(directionsDisplay.getMap(), legs[legs.length - 1].end_location, i, "some text for the " + i + "marker<br>" + legs[legs.length - 1].end_address);
                polyline.setMap(map);
                map.fitBounds(bounds);
                startAnimation();
              }
            }
          });
        })(k);
      }
    }
  };
  
  function updatePoly(d){
    // Mostrar una nueva polyine cada 20 vertices, de lo contrario se veria muy lento
    if (poly2.getPath().getLength() > 20) {
      poly2=new google.maps.Polyline([polyline.getPath().getAt(lastVertex-1)]);
    }
    if (polyline.GetIndexAtDistance(d) < lastVertex+2) {
      if (poly2.getPath().getLength()>1) {
        poly2.getPath().removeAt(poly2.getPath().getLength()-1)
      }
      poly2.getPath().insertAt(poly2.getPath().getLength(),polyline.GetPointAtDistance(d));
    } else {
      poly2.getPath().insertAt(poly2.getPath().getLength(),endLocation.latlng);
    }
  }
  function animate(d){
    if (d>eol) {
      map.panTo(endLocation.latlng);
      myMarker.setPosition(endLocation.latlng);
      return;
    }
    var p = polyline.GetPointAtDistance(d);
    map.panTo(p);
    myMarker.setPosition(p);
    updatePoly(d);
    timerHandle = setTimeout(animate, tick, d+step);
  }
  function startAnimation(){
    eol=polyline.Distance();
    map.setCenter(polyline.getPath().getAt(0));
    poly2 = new google.maps.Polyline({path: [polyline.getPath().getAt(0)], strokeColor:"#0000FF", strokeWeight:10});
    setTimeout(animate,2000, 100);  // Tiempo inicial para que se muestre el mapa
  }
}
var infowindow = new google.maps.InfoWindow({
  size: new google.maps.Size(150, 50)
});
function createMarker(map, latlng, label, html) {
  var contentString = '<b>' + label + '</b><br>' + html;
  var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    label: label.toString(),
    zIndex: Math.round(latlng.lat() * -100000) << 5
  });
  
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(contentString);
    infowindow.open(map, marker);
  });
  return marker;
}