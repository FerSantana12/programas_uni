$(document).ready(function(){
    var myOptions = {
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      map_canvas = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    
      address = 'managua'
      geocoder = new google.maps.Geocoder();
      geocoder.geocode( { 'address': address}, function(results) {
        map_canvas.setCenter(results[0].geometry.location);
      });
      funcion_ajax(map_canvas);
    
      /****EVENTO QUE PERMITE AL USUARIO GUARDAR LAS COORDENADAS EN LA BD
       * DE UN NUEVO PUNTO EN EL POLIGONO CUANDO DA CLICK AL MAPA */
      map_canvas.addListener("click", (mapsMouseEvent) => {
        opcion = 2;
        var geocerca;
        geocerca = mapsMouseEvent.latLng.toJSON();
        console.log(geocerca);
        $.ajax({
            type: 'post',
            url: 'bd/crud.php',
            datatype: 'json',
            data: {lat: geocerca.lat, lng: geocerca.lng, opcion: opcion},
            success: function (data) {
              var coords_geocerca = JSON.parse(data);
              console.log(coords_geocerca);
              var array_geocerca = [];
              coords_geocerca.forEach(element => {
                array_geocerca.push({lat: parseFloat(element.lat), lng: parseFloat(element.lgn) });      
              });
              const cercoPoligono = new google.maps.Polygon({
                paths: array_geocerca,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.35,
              });
              cercoPoligono.setMap(map_canvas);
              //FUNCION PARA REINICIAR LA GEOCERCA
              $("#restart").click(function(){
                restart(cercoPoligono);
              });
            }
        });
      });
    
    /** AQUI INICIA LA VALIDACION: PRIMERO AGARRO LAS COORDS DE LA GEOCERCA, SI EXISTEN,
   * LUEGO LAS COORDS DE LOS MARCADORES EXISTENTES, SI LOS HAY,
   *  CON LA FUNCION CHECKINPOLYGON VALIDO SI EXISTEN MARCADORES FUERA DE LA GEOCERCA*/

  //CABE DESTACAR QUE NI LA GEOCERCA, NI LOS MARCADORES SE CARGAN, PERO AUN
  //DEBO PASAR EL VALOR DE FECHA PARA QUE LA PETICION FUNCIONE CORRECTAMENTE
  var cercoPoligono_inicial, bounds;
  opcion = 3;
  var promise = $.ajax({
      type: 'post',
      url: 'bd/crud.php',
      datatype: 'json',
      data: {opcion: opcion},
      success: function (data) {
        var coords_geocerca_inicial = JSON.parse(data);
        var array_geocerca_inicial = [];
        coords_geocerca_inicial.forEach(element => {
          array_geocerca_inicial.push({lat: parseFloat(element.lat), lng: parseFloat(element.lgn) });      
        });
        cercoPoligono_inicial = new google.maps.Polygon({
          paths: array_geocerca_inicial,
          strokeColor: "#FF0000",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#FF0000",
          fillOpacity: 0.35,
        });
        bounds = new google.maps.LatLngBounds();
        for (var i=0; i<cercoPoligono_inicial.getPath().getLength(); i++) {
          bounds.extend(cercoPoligono_inicial.getPath().getAt(i));
        }
      }
  });
  promise.then(function(){
    opcion = 1;
    fecha = '2020-12-16';
    $.ajax({
      type: 'post',
      url: 'bd/crud.php',
      datatype: 'json',
      data: {fecha:fecha, opcion: opcion},
      success: function (data) {
        var coord_data = JSON.parse(data);
        var coord1 = [];
        coord_data.forEach(element => {
          coord1.push({lat: parseFloat(element.lat), lng: parseFloat(element.lng) });      
        });
        for(i = 0; i < coord1.length; i++){
          bounds.extend(coord1[i]);
           myMarker = new google.maps.Marker({
              position: coord1[i],
              label: i.toString(),
              //map: map
            });
            checkInPolygon(myMarker, cercoPoligono_inicial);
            
          }
      }
    });
  });
});

/************** FUNCION QUE SOLAMENTE MUESTRA LA GEOCERCA */
function funcion_ajax(map_canvas) {
    opcion = 3;
    $.ajax({
        type: 'post',
        url: 'bd/crud.php',
        datatype: 'json',
        data: {opcion: opcion},
        success: function (data) {
          var coords_geocerca_inicial = JSON.parse(data);
          var array_geocerca_inicial = [];
          coords_geocerca_inicial.forEach(element => {
            array_geocerca_inicial.push({lat: parseFloat(element.lat), lng: parseFloat(element.lgn) });      
          });
          console.log(array_geocerca_inicial);
          const cercoPoligono_inicial = new google.maps.Polygon({
            paths: array_geocerca_inicial,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
          });
          cercoPoligono_inicial.setMap(map_canvas);
          //FUNCION PARA REINICIAR LA GEOCERCA
          $("#restart").click(function(){
            restart(cercoPoligono_inicial);
          });
        }
    });
  }
  
  /*************** INTENTO DE FUNCION PARA VALIDAR GEOCERCA  ***************/
  function checkInPolygon(marker, polygon) {
  
    if (google.maps.geometry.poly.containsLocation(marker.getPosition(), polygon)) {
  
    } else {
    
      alert("Hay puntos fuera de la zona");
      //CON ESTA FUNCION SE DETIENE EL CICLO
      window.alert = function() { throw("alert called") } 
    }
  }

function restart(poligono) {
  poligono.setMap(null);
  opcion = 4;
  $.ajax({
    type:'post',
    url: 'bd/crud.php',
    datatype: 'json',
    data: {opcion: opcion},
    success: function(data){
      //console.log(data);
    }
  });
}