<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$fecha = (isset($_POST['fecha'])) ? $_POST['fecha'] : '';
$lat = (isset($_POST['lat'])) ? $_POST['lat'] : '';
$lng = (isset($_POST['lng'])) ? $_POST['lng'] : '';
$state = 'Activo'; $noState = 'Inactivo';

switch($opcion){
    case 1:
        $consulta = "SELECT * FROM `coords` WHERE `idUsuario` = 1 AND `date` LIKE '%".$fecha."%' ";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 2: 
        $consulta = "INSERT INTO `inv_geocerca`(`idemp`, `iduser`, `lat`, `lgn`, `state`) VALUES (1,1,'$lat', '$lng','$state')";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();

        $consulta = "SELECT * FROM `inv_geocerca` WHERE `idemp` = 1 AND `iduser` = 1 AND `state` = '$state'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 3:
        $consulta = "SELECT * FROM `inv_geocerca` WHERE `idemp` = 1 AND `iduser` = 1 AND `state` = '$state' ";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
    case 4:
        $consulta = "UPDATE `inv_geocerca` SET `state` = '$noState' WHERE `idemp` = 1 AND `iduser` = 1";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        break;
}

print json_encode($data, JSON_UNESCAPED_UNICODE);//envio el array final el formato json a AJAX
$conexion=null;