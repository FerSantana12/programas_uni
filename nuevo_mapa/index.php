<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Animacion V3</title>

<link rel="stylesheet" href="css/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>

<div id="tools">

	<button id="btnplay"><i class="fa fa-play" aria-hidden="true"></i> Calcular Ruta</button>
	<input id="fecha" name="fecha" type="date">
	<button id="restart">Reiniciar</button>
</div>

<div class="row">
	<div class="col">
		<div id="map" style="width:650px;height:500px;"></div>
	</div>
	<div class="col">
		<div id="map_canvas" style="width:650px;height:500px;"></div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbEx0w87e9r_5NsxE4A4S5olFa1nts4J0&callback=initmap&libraries=geometry&v=weekly"></script>
<script src="scripts/v3_epoly.js"></script>
<script src="scripts/main.js"></script>
<script src="scripts/main_geocerca.js"></script>
</body>
</html>
